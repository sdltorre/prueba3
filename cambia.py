def cambia(a,b):
    a.propietario , b.propietario  = b.propietario , a.propietario #asi conseguimos que cambien esos parámetros de la clase

class Vehículo:
    def __init__(self, p):
        self.propietario = p

p1 = Vehículo('pepe')
p2 = Vehículo('Ana')
#p3 = p1
#p4 = p2
#p1 = p2 #si quitamos esto saldria pepe y ana, con esto sale ana ana
#p2 = p3 # con esto sale ana y pepe 

#con esto imprime Juanita y Juanita
#p1.propietario ='Luis' #esta cambiando el valor de propietario de un objeto ya existente                                    
#p2.propietario = 'Juanita' 


#cabra = instancia = objeto
#referencias= cuerdas
#en las instancias no las copiamos
#los datos primitivos el valor o el dato o la letra, todo lo que no es instancia o referencia

cambia(p1,p2)
print('p1=', p1.propietario)
print('p2=', p2.propietario) #Aqui se queda p2 igual