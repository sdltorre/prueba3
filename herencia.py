class Animal:
    def __init__(self,p):
        self.peso = p

#herencia es construir una nueva idea a partir de la anterior, cada vez que decimos esto se parece a esto pero no es igual
#mamifero es un animal pero con algo especial
#forma de modelar a partir de otro modelo
        

class Mamífero(Animal):
    def __init__(self,p,c):
        super().__init__(p) #super = padre = animal (clase de la que viene, a partir de lo que estamos formando). Coge la p de la otra clase, asi que tmbn tendrán peso
        #llama al constructor de animal, primero construye la parte que tenga de animal y luego sigue con la clase mamiferos
        self.crias = c


p1 = Animal(30)
p2 = Mamífero(25, 8) #la clase mamifero ocupa más espacio que animal porque contiene partes de la clase Animal

