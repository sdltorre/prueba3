import unittest
from fraccion import Fracción

class TestFraccion(unittest.TestCase):
    def test_suma01(self):
        f1= Fracción(1,2)
        f2= Fracción(1,2)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 1)
        self.assertequal(suma.denominador, 1)

    def test_suma02(self):
        f1= Fracción(1,2)
        f2= Fracción(-1,2)

        suma = f1.suma(f2)

        self.assertEqual(suma.numerador, 0)
        self.assertequal(suma.denominador, 1)


if __name__ == '__main__':
    unittest.main()